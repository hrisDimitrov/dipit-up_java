package com.example.dipit.Controllers;

import com.example.dipit.Entities.Product;
import com.example.dipit.Entities.Service;
import com.example.dipit.Repositories.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/service")
@CrossOrigin(origins="*")
public class ServiceController {

    @Autowired
    public ServiceRepository serviceRepository;

    @GetMapping("")
    public List<Service> getProducts(){
        return serviceRepository.getService();
    }

    @PostMapping()
    public void postProduct(@RequestBody Service service){
        serviceRepository.postService(service);
    }
}
