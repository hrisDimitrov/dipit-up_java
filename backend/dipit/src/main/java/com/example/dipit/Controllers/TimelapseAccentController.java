package com.example.dipit.Controllers;


import com.example.dipit.Entities.TimelapseAccent;
import com.example.dipit.Repositories.TimelapseAccentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/timelapseAccent")
@CrossOrigin(origins="*")
public class TimelapseAccentController {

    @Autowired
    public TimelapseAccentRepository timelapseAccentRepository;

    @GetMapping("")
    public List<TimelapseAccent> getTimelapseAccents() {
        return timelapseAccentRepository.getTimelapseAccents();
    }

    @PostMapping()
    public void postTimelapseAccent(@RequestBody TimelapseAccent timelapseAccent) {
        timelapseAccentRepository.postTimelapseAccent(timelapseAccent);
    }
}
