package com.example.dipit.Controllers;

import com.example.dipit.Entities.Product;
import com.example.dipit.Entities.User;
import com.example.dipit.Repositories.MailGunRepository;
import com.example.dipit.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/users")
@CrossOrigin(origins="*")
public class UserController {
    @Autowired
    public UserRepository userRepository;
    public MailGunRepository mailGunRepository = new MailGunRepository();

    @GetMapping("")
    public List<User> getUsers(){
        System.out.println("we are here and we should have send mail");
        mailGunRepository.sendMailViaMailGun();
        System.out.println("is ok ? > " + mailGunRepository.sendMailViaMailGun().isOk());
        return userRepository.getUser();
    }

    @PostMapping()
    public void postUser(@RequestBody User user){
        System.out.println("post users");
        userRepository.postUser(user);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> get(@PathVariable Integer id) {
        try {
            List<User> user = userRepository.getUserById(id);
            return new ResponseEntity<User>(user.get(0), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> get(@PathVariable String username) {
        try {
            List<User> user = userRepository.getUserByUsername(username);
            return new ResponseEntity<User>(user.get(0), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/products-list/{username}")
    public List<Product> getProductsForUser(@PathVariable String username) {
        return userRepository.getProductsForCurrUser(username);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Integer id) {
        userRepository.deleteUser(id);
    }

}