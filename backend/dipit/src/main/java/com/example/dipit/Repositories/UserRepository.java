package com.example.dipit.Repositories;

import com.example.dipit.CustomPasswordEncoder;
import com.example.dipit.Entities.Product;
import com.example.dipit.Entities.User;
import com.example.dipit.Entities.UserInput;
import com.example.dipit.Mappers.ProductMapper;
import com.example.dipit.Mappers.UserMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class UserRepository {

    private JdbcTemplate jdbcTemplate;

    public  UserRepository() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/dipit_db");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "root" );
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<User> getUser(){
        String query = "select * from users";
        return jdbcTemplate.query(query,new UserMapper());
    }

    public List<User> getUserById(int id) {
        String query = "select * from users where id = '"+id+"'";
        return jdbcTemplate.query(query,new UserMapper());
    }

    public List<User> getUserByUsername(String username) {
        String query = "select * from users where id = '"+username+"'";
        return jdbcTemplate.query(query,new UserMapper());
    }

    public void postUser(User user) {
        String query = "insert into Users (username, password, email, phone_number, role)" +
                " values ('"+user.getUsername()+"','"+ CustomPasswordEncoder.hash(user.getPassword())+"','"+user.getEmail()+"','"+user.getPhone_number()+"','"+user.getRole()+"')";
        jdbcTemplate.update(query);
    }

    public void deleteUser(int id) {
        String query = "delete from users where id='"+id+"'";
        jdbcTemplate.update(query);
    }

    public List<Product> getProductsForCurrUser(String username) {
        String query = "select * from products as p JOIN users as u ON u.id = p.user_id WHERE u.username= '"+username+"';";
        return jdbcTemplate.query(query,new ProductMapper());
    }


    // this is user authorization
    public static class Boolean2{

        Boolean aBoolean;

        public Boolean2(Boolean aBoolean) {
            this.aBoolean = aBoolean;
        }



    }
    public boolean postUserLogin(UserInput uInput) {
        Boolean2 authorized=new Boolean2(false);
        User user = jdbcTemplate.query("select * from users where username='"+uInput.getUsername()+"'", new UserMapper()).get(0);

        String query = "select count(username) from users where username='"+uInput.getUsername()+"'";

        jdbcTemplate.query(query,(rs, rowNum) -> userCredentialsAreCorrect(rs, authorized, user , uInput));

        return authorized.aBoolean;
    }
    public boolean userCredentialsAreCorrect(ResultSet rs, Boolean2 authenticated, User user, UserInput uInput) throws SQLException {
        if( rs.getInt("count(username)") == 1){
            String query = "select * from users where username='"+user.getUsername()+"'";

            jdbcTemplate.query(query,(rs2, rowNum) -> checkPassword(rs2, authenticated, uInput) );

            return authenticated.aBoolean;
        }
        return false;
    }
    public boolean checkPassword(ResultSet rs, Boolean2 authenticated, UserInput uInput) throws SQLException {
        authenticated.aBoolean = rs.getString("password").equals(CustomPasswordEncoder.hash(uInput.getPassword()));
        return authenticated.aBoolean;
    }
}
