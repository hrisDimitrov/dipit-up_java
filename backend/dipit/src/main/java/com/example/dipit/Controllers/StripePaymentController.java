package com.example.dipit.Controllers;

import com.example.dipit.Components.StripeClient;
import com.example.dipit.Entities.StripeCustomerData;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/stripe")
@CrossOrigin(origins="*")
public class StripePaymentController {

    private StripeClient stripeClient;
    @Autowired
    void PaymentGatewayController(StripeClient stripeClient) {
        this.stripeClient = stripeClient;
    }
    @PostMapping("/charge")
    public Charge chargeCard(@RequestHeader(value="token") String token, @RequestHeader(value="amount") Double amount) throws Exception {
        return this.stripeClient.chargeNewCard(token, amount);
    }

    @PostMapping("/create-user")
    public StripeCustomerData index(@RequestBody StripeCustomerData stripeCustomerData) throws StripeException {
        Stripe.apiKey = "sk_test_51LBOibDCQLXkNOgFtmLBBIyVn2loRMNwPP4W1vkeXWBIZccFO1g7a0Deij8RqzjkKl0Qk4LUvhOWOoJoN5sSYnTJ00XwX1I0Si";
        Map<String, Object> params = new HashMap<>();
        params.put("name",stripeCustomerData.getName());
        params.put("email",stripeCustomerData.getEmail());

        Customer customer = Customer.create(params);
        stripeCustomerData.setCustomerId(customer.getId());
        return stripeCustomerData;
    }
}
