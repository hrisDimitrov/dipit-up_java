package com.example.dipit.Repositories;

import com.example.dipit.Entities.TimelapseAccent;
import com.example.dipit.Mappers.TimelapseAccentMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TimelapseAccentRepository {

    private JdbcTemplate jdbcTemplate;

    public  TimelapseAccentRepository() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/dipit_db");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "root" );
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public List<TimelapseAccent> getTimelapseAccents() {
        String query = "select * from TimelapseAccent";
        return jdbcTemplate.query(query,new TimelapseAccentMapper());
    }

    public void postTimelapseAccent(TimelapseAccent timelapseAccent) {
        String query = "insert into TimelapseAccent (title, description)" +
                " values ('"+timelapseAccent.getTitle()+"','"+timelapseAccent.getDescription()+"')";
        jdbcTemplate.update(query);
    }
}
