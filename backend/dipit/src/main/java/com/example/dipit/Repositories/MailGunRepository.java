package com.example.dipit.Repositories;

import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import net.sargue.mailgun.Response;
public class MailGunRepository {

    public MailGunRepository(){};
    Configuration configuration = new Configuration()
            .domain("dipit-up")
            .apiUrl("https://api.mailgun.net/v3/sandbox8bf9493417e0417ca4bc5bb8e2b1cd2e.mailgun.org/messages")
            .apiKey("key-93c6b1b2fea8b6aca4cbfd2d557f4c99-50f43e91-af464fa2")
            .from("Hristos Dimitrov TEST", "hristosdimirov16@gmail.com");

    public Response sendMailViaMailGun(){
        Mail mail = Mail.using(configuration).to("hristosdimitrov112@gmail.com").subject("This is the subject").text("Hello world!").build();
        Response response = mail.send();
        return response;
    }


}

