package com.example.dipit.Mappers;

import com.example.dipit.Entities.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        final Product product = new Product();
        product.setId(Long.parseLong(rs.getString("id")));
        product.setTitle(rs.getString("title"));
        product.setDescription(rs.getString("description"));
        product.setPrice(Long.parseLong(rs.getString("price")));
        product.setImageUrl(rs.getString("image_url"));
        product.setFeatured(rs.getBoolean("isFeatured"));
        return product;
    }
}
