package com.example.dipit.Entities;

public class UserInput {
    public final String username;
    public final String password;

    public UserInput(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
