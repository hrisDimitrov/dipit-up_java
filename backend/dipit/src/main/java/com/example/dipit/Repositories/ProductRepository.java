package com.example.dipit.Repositories;

import com.example.dipit.Entities.Product;
import com.example.dipit.Mappers.ProductMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepository {
    private JdbcTemplate jdbcTemplate;

    public  ProductRepository() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/dipit_db");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "root" );
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Product> getProducts() {
        String query = "select * from products";
        return jdbcTemplate.query(query,new ProductMapper());
    }
    public List<Product> getFeaturedProducts() {
        String query = "select * from products where isFeatured = true";
        return jdbcTemplate.query(query,new ProductMapper());
    }
    public void postProduct(Product product) {
        String query = "insert into Products (title, description, price, image_url)" +
                " values ('"+product.getTitle()+"','"+product.getDescription()+"',"+product.getPrice()+",'"+product.getImageUrl()+"')";
        jdbcTemplate.update(query);
    }
}
