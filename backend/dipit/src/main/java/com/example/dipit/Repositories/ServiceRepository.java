package com.example.dipit.Repositories;

import com.example.dipit.Entities.Service;
import com.example.dipit.Mappers.ServiceMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepository {

    private JdbcTemplate jdbcTemplate;

    public  ServiceRepository() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/dipit_db");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "root" );
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public List<Service> getService() {
        String query = "select * from services";
        return jdbcTemplate.query(query,new ServiceMapper());
    }

    public void postService(Service service) {
        String query = "insert into services (title, description)" +
                " values ('"+service.getTitle()+"','"+service.getDescription()+"')";
        jdbcTemplate.update(query);
    }
}
