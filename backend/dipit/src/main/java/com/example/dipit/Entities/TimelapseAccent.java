package com.example.dipit.Entities;

import javax.persistence.*;
import java.sql.Date;

@Table(name = "TimelapseAccent")
public class TimelapseAccent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "time_stamp")
    private Date timeStamp;

    public TimelapseAccent(){};

    public TimelapseAccent(long id, String title, String description, Date timeStamp) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.timeStamp = timeStamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
