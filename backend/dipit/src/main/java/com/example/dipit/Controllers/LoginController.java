package com.example.dipit.Controllers;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.example.dipit.Entities.UserInput;
import com.example.dipit.Entities.JwtUser;
import com.example.dipit.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@CrossOrigin(origins="*")
public class LoginController {
    @Autowired
    public UserRepository userRepository;

    @PostMapping("api/login")
    public JwtUser login(@RequestBody UserInput user) {
        String token = getJWTToken(user.username);
        JwtUser jwtUser = new JwtUser(user.username,token);
        boolean authorized = userRepository.postUserLogin(user);
        if (authorized){
//            return ResponseEntity.ok()
//                    .header(
//                            HttpHeaders.AUTHORIZATION,
//                            jwtUser.token
//                    ).body(jwtUser);
           return jwtUser;
        }
        return new JwtUser(null,null);
        //return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    private String getJWTToken(String username) {
        String secretKey = "mySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKeymySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("softtekJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}