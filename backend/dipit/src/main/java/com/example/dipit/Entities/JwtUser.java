package com.example.dipit.Entities;

public class JwtUser {
    public final String username;

    public final String token;

    public JwtUser(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }
}
