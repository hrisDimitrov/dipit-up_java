package com.example.dipit.Controllers;


import com.example.dipit.Entities.Product;
import com.example.dipit.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
@CrossOrigin(origins="*")
public class ProductController {

    @Autowired
    public ProductRepository productRepository;

    @GetMapping("")
    public List<Product> getProducts(){
        return productRepository.getProducts();
    }

    @GetMapping("/featured")
    public List<Product> getFeaturedProducts(){
        return productRepository.getFeaturedProducts();
    }

    @PostMapping()
    public void postProduct(@RequestBody Product product){
        System.out.println("posting product now");
        productRepository.postProduct(product);
    }
}
