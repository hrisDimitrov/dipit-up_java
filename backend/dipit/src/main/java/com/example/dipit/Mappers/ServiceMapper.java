package com.example.dipit.Mappers;

import com.example.dipit.Entities.Service;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ServiceMapper implements RowMapper<Service> {

    @Override
    public Service mapRow(ResultSet rs, int rowNum) throws SQLException {
    final Service service = new Service();
        service.setId(Long.parseLong(rs.getString("id")));
        service.setTitle(rs.getString("title"));
        service.setDescription(rs.getString("description"));
        return service;
    }
}
