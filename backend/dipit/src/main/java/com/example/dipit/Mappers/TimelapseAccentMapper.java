package com.example.dipit.Mappers;

import com.example.dipit.Entities.TimelapseAccent;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TimelapseAccentMapper implements RowMapper<TimelapseAccent> {

    @Override
    public TimelapseAccent mapRow(ResultSet rs, int rowNum) throws SQLException {
        final TimelapseAccent timelapseAccent = new TimelapseAccent();
        timelapseAccent.setId(Long.parseLong(rs.getString("id")));
        timelapseAccent.setTitle(rs.getString("title"));
        timelapseAccent.setDescription(rs.getString("description"));
        timelapseAccent.setTimeStamp(rs.getDate("time_stamp"));
        return timelapseAccent;
    }
}
