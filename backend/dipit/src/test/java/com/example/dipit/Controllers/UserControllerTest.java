package com.example.dipit.Controllers;

import com.example.dipit.Entities.User;
import com.example.dipit.Repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@DataJdbcTest
@ActiveProfiles("test")
public class UserControllerTest {

    @Autowired
    public UserRepository userRepository;

    @Test
    public void getUsers(){
        User user = new User("Hristos Dimitrov","hris123", "hristosdimitrov112@gmail.com", "0898558929", "ADMIN");
        List<User> expectedList = userRepository.getUser();

        Assert.assertEquals(expectedList.get(0), user);;
    }
    private Stream<User> entities() {
        return Stream.of(
                new User("Hristos Dimitrov","hris123", "hristosdimitrov112@gmail.com", "0898558929", "ADMIN"),
                new User("Hristos Dimitrov","hris123", "hristosdimitrov112@gmail.com", "0898558929", "ADMIN"),
                new User("Hristos Dimitrov","hris123", "hristosdimitrov112@gmail.com", "0898558929", "ADMIN")
        );
    }
}
