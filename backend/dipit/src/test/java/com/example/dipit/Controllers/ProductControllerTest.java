package com.example.dipit.Controllers;

import com.example.dipit.Entities.Product;
import com.example.dipit.Entities.User;
import com.example.dipit.Repositories.ProductRepository;
import com.example.dipit.Repositories.UserRepository;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;

class ProductControllerTest {

    @Autowired
    public ProductRepository productRepository = new ProductRepository();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getProducts() {
        Product product = new Product(1L,1L,"ihpone","13 pro max", 1500L, "https://s13emagst.akamaized.net/products/40685/40684442/images/res_8a64b24b271b1691900872dc60fc1e66.jpg",true);
        List<Product> expectedList = productRepository.getProducts();

        Assert.assertEquals(expectedList.get(0), product);;
    }

    @Test
    void getFeaturedProducts() {
    }

    @Test
    void postProduct() {
    }
}