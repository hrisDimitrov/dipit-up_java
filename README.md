# Web Service Project

Used Java Spring Boot when implementing the Back-end service, Reactjs for the implementation of the Front-end and MySql for database. Got 3-rd party Api's (Stripe). Spring Security.

## What is it about

It is a new platform about Selling and Buying all kind of products.

## Preview

![](https://i.ibb.co/kHxrc2x/1-git.jpg)

## Usage

You need to Register and Login afterward in order to be able to Add Product into the site. You will be able to see all the products you listed in the site in your Profile, which will be visible when you login with existing credentials.

## Installation

WINDOWS
Use the git clone method https://git.trading212.io/hristos.dimitrov/web-service-project.git .
After that for the front-end use "npm install" to install all the dependencies. For the Back-end use InteliJ idea to build the maven Project and run it.
MacOS

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Project status

In the future there will be a cart implemented so you can pay in bulk for all the products you have chosen to buy. Front-end design will be changed, most of the things will change places, new payment methods will be added as well as orders. Categories will be included too.

## Name
Dipit-up

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
-Hristos Dimitrov

## License
[MIT](https://choosealicense.com/licenses/mit/)
