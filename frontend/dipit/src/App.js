import Login from "./components/Login";
import Logout from "./components/Logout";
import Products from "./components/Products";
import Welcome from "./components/Welcome";
import Register from "./components/Registration";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PrivacyPolicy from "./components/PrivacyPolicy";
import TermsAndConditions from "./components/TermsAndConditions";
import AddProduct from "./components/AddProduct";
import MyProfile from "./components/MyProfile";

function App() {
  return (
    <div>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/" exact>
              <Welcome />
            </Route>
            <Route path="/register" exact>
              <Register />
            </Route>
            <Route path="/login" exact>
              <Login />
            </Route>
            <Route path="/logout" exact>
              <Logout />
            </Route>
            <Route path="/products" exact>
              <Products />
            </Route>
            <Route path="/privacy-policy" exact>
              <PrivacyPolicy />
            </Route>
            <Route path="/terms-and-conditions" exact>
              <TermsAndConditions />
            </Route>
            <Route path="/post-product" exact>
              <AddProduct />
            </Route>MyProfile
            <Route path="/my-profile" exact>
              <MyProfile />
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
