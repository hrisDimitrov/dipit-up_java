import "../assets/css/styles_new_temp.css";
import "../assets/js/scripts_temp";

import axios from "axios";
import { Component } from "react";
// import { useState } from "react";

import iphone from "../assets/img/gallery/iphone.jpg";

export default class Products extends Component {
  // const [counter, setCounter] = useState(0);

  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    axios.get("http://localhost:8080/products").then((response) => {
      this.setState({ products: response.data });
    });
  }

  render() {
    return (
      <>
        <div class="row">
          {this.state.products.map((product) => (
            <div class="col-lg-4 col-sm-6 mb-4">
              <div class="portfolio-item">
                <a
                  class="portfolio-link"
                  data-bs-toggle="modal"
                  href="#portfolioModal1"
                >
                  <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                      <i class="fas fa-plus fa-3x"></i>
                    </div>
                  </div>
                  <img class="img-fluid" src={product.imageUrl} alt="..." />
                </a>
                <div class="portfolio-caption">
                  <div class="portfolio-caption-heading">{product.title}</div>
                  <div class="portfolio-caption-subheading text-muted">
                    {product.description}
                  </div>
                  <div class="portfolio-price">{product.price} ЛВ.</div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  }
}
