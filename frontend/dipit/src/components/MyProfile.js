import React from "react";
import { Component } from "react";
import axios from "axios";

export default class MyProfile extends Component {
  // const [counter, setCounter] = useState(0);

  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    let username = localStorage.getItem("username");
    axios
      .get("http://localhost:8080/users/products-list/" + username)
      .then((response) => {
        this.setState({ products: response.data });
      });
  }

  render() {
    return (
      <>
        <section class="page-section bg-light" id="portfolio">
          <div class="container">
            <div class="text-center">
              <h2 class="section-heading text-uppercase">
                Hello, {localStorage.getItem("username")}
              </h2>
              <h3 class="section-subheading text-muted">
                These are your products on sale now.
              </h3>
            </div>
          </div>

          <div class="row">
            {this.state.products.map((product) => (
              <div class="col-lg-4 col-sm-6 mb-4">
                <div class="portfolio-item">
                  <a
                    class="portfolio-link"
                    data-bs-toggle="modal"
                    href="#portfolioModal1"
                  >
                    <div class="portfolio-hover">
                      <div class="portfolio-hover-content">
                        <i class="fas fa-plus fa-3x"></i>
                      </div>
                    </div>
                    <img class="img-fluid" src={product.imageUrl} alt="..." />
                  </a>
                  <div class="portfolio-caption">
                    <div class="portfolio-caption-heading">{product.title}</div>
                    <div class="portfolio-caption-subheading text-muted">
                      {product.description}
                    </div>
                    <div class="portfolio-price">{product.price} ЛВ.</div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>
      </>
    );
  }
}
