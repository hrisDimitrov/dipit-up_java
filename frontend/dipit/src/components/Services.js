import axios from "axios";
import { Component } from "react";

export default class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      services: [],
    };
  }

  componentDidMount() {
    axios.get("http://localhost:8080/service").then((response) => {
      this.setState({ services: response.data });
    });
  }

  render() {
    return (
      <>
        {this.state.services.map((service) => (
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="my-3">{service.title}</h4>
            <p class="text-muted">
              {service.description}
            </p>
          </div>
        ))}
      </>
    );
  }
}
