import axios from "axios";
import { Component } from "react";

import dipit from "../assets/img/dipit-logo.jpg";

export default class TimelapseAccents extends Component {
    
    constructor(props) {
        super(props);
        this.state = { 
            timelapseAccents: [],
        };
    }
    
    componentDidMount() {
    axios.get("http://localhost:8080/timelapseAccent").then((response) => {
      this.setState({ timelapseAccents: response.data });
    });
  }


  render() {
    return (
      <>
          {this.state.timelapseAccents.map((timelapseAccent) => (
            <li>
              <div class="timeline-image">
                <img
                  class="rounded-circle img-fluid"
                  src={dipit}
                  alt="..."
                />
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>{timelapseAccent.timeStamp}</h4>
                  <h4 class="subheading">{timelapseAccent.title}</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">
                    {timelapseAccent.description}
                  </p>
                </div>
              </div>
            </li>
          ))}
      </>
    );
  }
}
