import React from "react";

function Register() {
  function Logout() {
    localStorage.clear();
    window.location.href = "http://localhost:3000";
  }

  return (
    <div id="loginBox">
      <h1>Sure you want to Log out?</h1>
      <div className="actions">
        <button className="btn" type="submit" onClick={Logout}>
          Log out
        </button>
      </div>
    </div>
  );
}

export default Register;
