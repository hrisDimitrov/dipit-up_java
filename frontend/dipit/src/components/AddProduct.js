import axios from "axios";
import { useState } from "react";
import "../css/registration.css";

function AddProduct() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");

  async function addProduct(event) {
    event.preventDefault();
    try {
      await axios.post("http://localhost:8080/products", {
        title: title,
        description: description,
        price: price,
        image: image,
      });
      alert("Product Posted Successfully");
      setTitle("");
      setDescription("");
      setPrice("");
      setImage("");
      window.location.reload(true);
      window.location.href = "http://localhost:3000/products";
    } catch (err) {
      alert(err);
    }
  }
  return (
    <div className="register-container">
      <form className="register-form" onSubmit={addProduct}>
        <br></br>
        <h1>Add Product</h1>
        <p>Fill in the Information Below</p>

        <input
          type="text"
          name="title"
          placeholder="Title"
          onChange={(event) => {
            setTitle(event.target.value);
          }}
        />

        <input
          type="text"
          name="description"
          placeholder="Description"
          onChange={(event) => {
            setDescription(event.target.value);
          }}
        />

        <input
          type="text"
          name="price"
          placeholder="Price"
          onChange={(event) => {
            setPrice(event.target.value);
          }}
        />

        <input
          type="text"
          name="image"
          placeholder="Image URL"
          onChange={(event) => {
            setImage(event.target.value);
          }}
        />

        <button type="submit">Post!</button>
      </form>
    </div>
  );
}

export default AddProduct;