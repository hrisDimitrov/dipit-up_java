import "../assets/css/styles_new_temp.css";
import "../assets/js/scripts_temp";

import Stripe from "react-stripe-checkout";
import axios from "axios";

import author1 from "../assets/img/gallery/author-1.png";
import author2 from "../assets/img/gallery/author-2.png";
import author3 from "../assets/img/gallery/author-3.png";
import FeaturedProducts from "./FeaturedProducts";
import TimelapseAccents from "./TimelapseAccents";
import Services from "./Services";

function Welcome() {
  async function handleToken(token) {
    console.log(token);
    await axios
      .post("http://localhost:8080/api/payment/charge", "", {
        headers: {
          token: token.id,
          amount: 500,
          currency: "USD",
        },
      })
      .then(() => {
        alert("Payment Success");
      })
      .catch((error) => {
        alert(error);
      });
  }
  return (
    <>
      <header class="masthead">
        <div class="container">
          <div class="masthead-subheading">Dipit!</div>
          <div class="masthead-heading text-uppercase">
            Sell and Buy the new way!
          </div>
          <a class="btn btn-primary btn-xl text-uppercase" href="#services">
            Tell Me More
          </a>
        </div>
      </header>

      <section class="page-section" id="services">
        <div class="container">
          <div class="text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
            <h3 class="section-subheading text-muted">All our services.</h3>
          </div>
          <div class="row text-center">
            <Services />
          </div>
        </div>
      </section>

      <section class="page-section bg-light" id="portfolio">
        <div class="container">
          <div class="text-center">
            <h2 class="section-heading text-uppercase">Featured products</h2>
            <h3 class="section-subheading text-muted">
              Top 10 products witch are on sale now.
            </h3>
          </div>
        </div>
        {localStorage.getItem("jwt") ? (
          <>
            <Stripe stripeKey="pk_test_51LBOibDCQLXkNOgFwxsLWzCPuGrbo4uiomhLnISyjZl5QSPuKH271JK7dq5zfQ7ShZLYxvCiKyBNTnjrcFI9tj5m00BQlvjLOe" token={handleToken}/>
            <a className="addProductA" href="http://localhost:3000/post-product">
              <button className="addProductBtn">+ADD PRODUCT</button>
            </a>
          </>
        ) : (
          <>
            <h1>Register/Login to add products.</h1>
          </>
        )}
        <FeaturedProducts />
      </section>

      <section class="page-section" id="about">
        <div class="container">
          <div class="text-center">
            <h2 class="section-heading text-uppercase">About</h2>
            <h3 class="section-subheading text-muted">
              Lorem ipsum dolor sit amet consectetur.
            </h3>
          </div>
          <ul class="timeline">
            <TimelapseAccents />
            <li>
              <div class="timeline-image">
                <img
                  class="rounded-circle img-fluid"
                  src="assets/img/about/1.jpg"
                  alt="..."
                />
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>2009-2011</h4>
                  <h4 class="subheading">Our Humble Beginnings</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Sunt ut voluptatum eius sapiente, totam reiciendis
                    temporibus qui quibusdam, recusandae sit vero unde, sed,
                    incidunt et ea quo dolore laudantium consectetur!
                  </p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <img
                  class="rounded-circle img-fluid"
                  src="assets/img/about/2.jpg"
                  alt="..."
                />
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>March 2011</h4>
                  <h4 class="subheading">An Agency is Born</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Sunt ut voluptatum eius sapiente, totam reiciendis
                    temporibus qui quibusdam, recusandae sit vero unde, sed,
                    incidunt et ea quo dolore laudantium consectetur!
                  </p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <h4>
                  Be Part
                  <br />
                  Of Our
                  <br />
                  Story!
                </h4>
              </div>
            </li>
          </ul>
        </div>
      </section>

      <section class="page-section bg-light" id="team">
        <div class="container">
          <div class="text-center">
            <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
            <h3 class="section-subheading text-muted">One man army.</h3>
          </div>
          <div class="row">
            <div class="col-lg-4">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src={author1} alt="..." />
                <h4>Preslav</h4>
                <p class="text-muted">Gave me the task.</p>
                <a
                  class="btn btn-dark btn-social mx-2"
                  href="https://www.trading212.com/"
                  aria-label="Parveen Anand Twitter Profile"
                >
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
            </div>

            <div class="col-lg-4">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src={author2} alt="..." />
                <h4>Hristos Dimitrov</h4>
                <p class="text-muted">
                  Back-end Dev. Front-end Dev. Architecture design
                </p>
                <a
                  class="btn btn-dark btn-social mx-2"
                  href="https://www.trading212.com/"
                  aria-label="Larry Parker Twitter Profile"
                >
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="team-member">
                <img class="mx-auto rounded-circle" src={author3} alt="..." />
                <h4>Trading 212</h4>
                <p class="text-muted">Payed me to develop</p>
                <a
                  class="btn btn-dark btn-social mx-2"
                  href="https://www.trading212.com/"
                  aria-label="Larry Parker Twitter Profile"
                >
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 mx-auto text-center">
              <p class="large text-muted">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut
                eaque, laboriosam veritatis, quos non quis ad perspiciatis,
                totam corporis ea, alias ut unde.
              </p>
            </div>
          </div>
        </div>
      </section>

      <section class="page-section" id="contact">
        <div class="container">
          <div class="text-center">
            <h2 class="section-heading text-uppercase">Contact Us</h2>
            <h3 class="section-subheading text-muted">
              If you have any questions about how our site and services work.
            </h3>
          </div>

          <form id="contactForm" data-sb-form-api-token="API_TOKEN">
            <div class="row align-items-stretch mb-5">
              <div class="col-md-6">
                <div class="form-group">
                  <input
                    class="form-control"
                    id="name"
                    type="text"
                    placeholder="Your Name *"
                    data-sb-validations="required"
                  />
                  <div
                    class="invalid-feedback"
                    data-sb-feedback="name:required"
                  >
                    A name is required.
                  </div>
                </div>
                <div class="form-group">
                  <input
                    class="form-control"
                    id="email"
                    type="email"
                    placeholder="Your Email *"
                    data-sb-validations="required,email"
                  />
                  <div
                    class="invalid-feedback"
                    data-sb-feedback="email:required"
                  >
                    An email is required.
                  </div>
                  <div class="invalid-feedback" data-sb-feedback="email:email">
                    Email is not valid.
                  </div>
                </div>
                <div class="form-group mb-md-0">
                  <input
                    class="form-control"
                    id="phone"
                    type="tel"
                    placeholder="Your Phone *"
                    data-sb-validations="required"
                  />
                  <div
                    class="invalid-feedback"
                    data-sb-feedback="phone:required"
                  >
                    A phone number is required.
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group form-group-textarea mb-md-0">
                  <textarea
                    class="form-control"
                    id="message"
                    placeholder="Your Message *"
                    data-sb-validations="required"
                  ></textarea>
                  <div
                    class="invalid-feedback"
                    data-sb-feedback="message:required"
                  >
                    A message is required.
                  </div>
                </div>
              </div>
            </div>

            <div class="d-none" id="submitSuccessMessage">
              <div class="text-center text-white mb-3">
                <div class="fw-bolder">Form submission successful!</div>
              </div>
            </div>

            <div class="d-none" id="submitErrorMessage">
              <div class="text-center text-danger mb-3">
                Error sending message!
              </div>
            </div>

            <div class="text-center">
              <button
                class="btn btn-primary btn-xl text-uppercase disabled"
                id="submitButton"
                type="submit"
              >
                Send Message
              </button>
            </div>
          </form>
        </div>
      </section>

      <div
        class="portfolio-modal modal fade"
        id="portfolioModal1"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="close-modal" data-bs-dismiss="modal">
              <img src="assets/img/close-icon.svg" alt="Close modal" />
            </div>
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-lg-8">
                  <div class="modal-body">
                    <h2 class="text-uppercase">Project Name</h2>
                    <p class="item-intro text-muted">
                      Lorem ipsum dolor sit amet consectetur.
                    </p>
                    <img
                      class="img-fluid d-block mx-auto"
                      src="assets/img/portfolio/1.jpg"
                      alt="..."
                    />
                    <p>
                      Use this area to describe your project. Lorem ipsum dolor
                      sit amet, consectetur adipisicing elit. Est blanditiis
                      dolorem culpa incidunt minus dignissimos deserunt repellat
                      aperiam quasi sunt officia expedita beatae cupiditate,
                      maiores repudiandae, nostrum, reiciendis facere nemo!
                    </p>
                    <ul class="list-inline">
                      <li>
                        <strong>Client:</strong>
                        Threads
                      </li>
                      <li>
                        <strong>Category:</strong>
                        Illustration
                      </li>
                    </ul>
                    <button
                      class="btn btn-primary btn-xl text-uppercase"
                      data-bs-dismiss="modal"
                      type="button"
                    >
                      <i class="fas fa-xmark me-1"></i>
                      Close Project
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Welcome;
