import axios from "axios";
import { useState } from "react";
import "../css/registration.css";

function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [role, setRole] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      await axios.post("http://localhost:8080/users", {
        username: username,
        password: password,
        email: email,
        phone: phone,
        role: role,
      });
      alert("User Registation Successfully");
      setUsername("");
      setPassword("");
      setEmail("");
      setPhone("");
      setRole("");
      window.location.reload(true);
      window.location.href = "http://localhost:3000/login";
    } catch (err) {
      alert(err);
    }
  }
  return (
    <div className="register-container">
      <form className="register-form" onSubmit={handleSubmit}>
        <br></br>
        <h1>Register</h1>
        <p>Fill in the Information Below</p>

        <input
          type="text"
          name="username"
          placeholder="username"
          onChange={(event) => {
            setUsername(event.target.value);
          }}
        />

        <input
          type="password"
          name="password"
          placeholder="password"
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />

        <input
          type="text"
          name="email"
          placeholder="email"
          onChange={(event) => {
            setEmail(event.target.value);
          }}
        />

        <input
          type="text"
          name="phone"
          placeholder="phone"
          onChange={(event) => {
            setPhone(event.target.value);
          }}
        />

        <input
          type="text"
          name="role"
          placeholder="role"
          onChange={(event) => {
            setRole(event.target.value);
          }}
        />

        <button type="submit">Register</button>
      </form>
    </div>
  );
}

export default Register;
