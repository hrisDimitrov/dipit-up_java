import { useState } from "react";
import React from "react";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function sendLoginRequest() {
    const reqBody = {
      username: username,
      password: password,
    };

    fetch("http://localhost:8080/api/login", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify(reqBody),
    })
      .then((response) => response.json())
      .then((data) => {
        localStorage.setItem("jwt", data.token);
        localStorage.setItem("username", data.username);
      });

    if (localStorage.getItem("jwt") !== null) {
      window.location.reload(true);
      window.location.href = "http://localhost:3000";
    } else {
      window.location.href = "http://localhost:3000/login";
    }
  }

  return (
    <form onSubmit={sendLoginRequest}>
      <div id="loginBox">
        <h1>Sign in!</h1>
        <div className="card">
          <h3>Username:</h3>
          <input
            type="text"
            id="username"
            name="username"
            value={username}
            placeholder="Enter Username..."
            onChange={(e) => setUsername(e.target.value)}
          ></input>
          <h3>Password:</h3>
          <input
            type="password"
            id="password"
            name="password"
            value={password}
            placeholder="Enter Password..."
            onChange={(e) => setPassword(e.target.value)}
          ></input>
          <div className="actions">
            <button className="btn" type="submit">
              Sing in
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Login;
