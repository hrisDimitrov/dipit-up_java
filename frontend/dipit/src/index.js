import React from "react";
import ReactDOM from "react-dom/client";
import "./css/index.css";
import App from "./App";
import Header from "./Header";
import Footer from "./Footer";

const root = ReactDOM.createRoot(document.getElementById("root"));
const header = ReactDOM.createRoot(document.getElementById("header"));
const footer = ReactDOM.createRoot(document.getElementById("footer"));
root.render(<App />);
header.render(<Header />);
footer.render(<Footer />);
