import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


function Footer() {
  return (
    <footer class="footer py-4">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 text-lg-start">
            Copyright &copy; Hris's Website 2022
          </div>
          <div class="col-lg-4 my-3 my-lg-0">
            <a
              class="btn btn-dark btn-social mx-2"
              href="#!"
              aria-label="Twitter"
            >
              <i class="fab fa-twitter"></i>
            </a>
            <a
              class="btn btn-dark btn-social mx-2"
              href="#!"
              aria-label="Facebook"
            >
              <FontAwesomeIcon icon="fab fa-facebook-square" />
            </a>
            <a
              class="btn btn-dark btn-social mx-2"
              href="#!"
              aria-label="LinkedIn"
            >
              <i class="fab fa-linkedin-in"></i>
            </a>
          </div>
          <div class="col-lg-4 text-lg-end">
            <a
              class="link-dark text-decoration-none me-3"
              href="http://localhost:3000/privacy-policy"
            >
              Privacy Policy
            </a>
            <a
              class="link-dark text-decoration-none"
              href="http://localhost:3000/terms-and-conditions"
            >
              Terms of Use
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
