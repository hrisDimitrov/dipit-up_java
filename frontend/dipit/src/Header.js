import logo from "./assets/img/dipit-logo.jpg";

function Header() {
  return (
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="/">
          <img src={logo} alt="Dipit" />
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          Menu
          <i class="fas fa-bars ms-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="#services">
                {" "}
                Services{" "}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#portfolio">
                {" "}
                Top Products{" "}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">
                {" "}
                About{" "}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#team">
                {" "}
                Team{" "}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact">
                {" "}
                Contact{" "}
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost:3000/products">
                All Products
              </a>
            </li>
            {localStorage.getItem("jwt") ? (
                <>
            <li class="nav-item">
            <a class="nav-link" href="http://localhost:3000/my-profile">
              My Profile
            </a>
          </li>
              <li class="nav-item">
                <a class="nav-link" href="http://localhost:3000/logout">
                  Hello, {localStorage.getItem("username")} Log out
                </a>
              </li>
              </>
            ) : (
              <>
                <li class="nav-item">
                  <a class="nav-link" href="http://localhost:3000/register">
                    Register
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="http://localhost:3000/login">
                    Log in
                  </a>
                </li>
              </>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Header;
